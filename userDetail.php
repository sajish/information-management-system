<?php
	//error_reporting(0); 
	session_start();
	require_once('connection.php');
	require_once('commonFunctions.php');
	CheckSession(1);
	$thisPage = "User";
	$checked="checked";
	$chkMale="";
	$chkFemale="";
	$selAdmin="";
	$selMod="";
	$selStaff="";
	$strIuQuery="";
	
	$userId = isset($_GET["userId"]) ? Sanitize($_GET["userId"]) : '0'; 
	$action = isset($_POST['action']) ? Sanitize($_POST['action']) : ''; 
	$reqInitials = isset($_POST['txtInitials']) ? Sanitize($_POST['txtInitials']) : ''; 
	$reqFirstName = isset($_POST['txtFirstName']) ? Sanitize($_POST['txtFirstName']) : ''; 
	$reqLastName = isset($_POST['txtLastName']) ? Sanitize($_POST['txtLastName']) : ''; 
	$reqAddress = isset($_POST['txtAddress']) ? Sanitize($_POST['txtAddress']) : ''; 
	$reqPhoneNumber = isset($_POST['txtPhnNumber']) ? Sanitize($_POST['txtPhnNumber']) : ''; 
	$reqMobileNumber = isset($_POST['txtMobNumber']) ? Sanitize($_POST['txtMobNumber']) : ''; 
	$reqGender = isset($_POST['radGender']) ? Sanitize($_POST['radGender']) : ''; 
	$reqEmail = isset($_POST['txtEmail']) ? Sanitize($_POST['txtEmail']) : ''; 
	$reqSalary = isset($_POST['txtSpm']) ? Sanitize($_POST['txtSpm']) : ''; 
	$reqDob = isset($_POST['txtDob']) ? Sanitize($_POST['txtDob']) : ''; 
	$reqIsActive = isset($_POST['chkIsActive']) ? Sanitize($_POST['chkIsActive']) : '';
	$reqIsActive = $reqIsActive == 'on' ? '1' : '0';
	$reqUserName = isset($_POST['txtUserName']) ? Sanitize($_POST['txtUserName']) : ''; 
	$reqUserType = isset($_POST['selectUserType']) ? Sanitize($_POST['selectUserType']) : ''; 
	$reqPassword = isset($_POST['txtPassword']) ? Sanitize($_POST['txtPassword']) : '';
	$reqPassword = $reqPassword == '' ? '' : md5($reqPassword);
	
	if($action == "save")
	{
		if($userId == "0")
		{
			$strIuQuery = "INSERT INTO Users(Initials,FirstName,LastName,Address,PhoneNumber,MobileNumber,Gender,Email,SalaryPerMonth,DateOfBirth,IsActiveUser,UserName,UserTypeId,Password)";
			$strIuQuery .= " VALUES('".$reqInitials."','".$reqFirstName."','".$reqLastName."','".$reqAddress."','".$reqPhoneNumber."','".$reqMobileNumber."','".$reqGender."','".$reqEmail."',";
			$strIuQuery .= "'".$reqSalary."','".$reqDob."','".$reqIsActive."','".$reqUserName."','".$reqUserType."','".$reqPassword."')";
			
			mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
			$userId = mysql_insert_id();
			header("location: userDetail.php?userId=".$userId);
			exit();
		}
		else
		{
			$strIuQuery = "UPDATE Users SET Initials='".$reqInitials."',FirstName='".$reqFirstName."',LastName='".$reqLastName."',Address='".$reqAddress."',PhoneNumber='".$reqPhoneNumber."',";
			$strIuQuery .= "MobileNumber='".$reqMobileNumber."',Gender='".$reqGender."',Email='".$reqEmail."',SalaryPerMonth='".$reqSalary."',DateOfBirth='".$reqDob."',";
			$strIuQuery .= "IsActiveUser='".$reqIsActive."',UserName='".$reqUserName."',UserTypeId='".$reqUserType."'";
			if($reqPassword != "")
			{
				$strIuQuery .= ",Password='".$reqPassword."'";
			}
			$strIuQuery .= " WHERE UserId = ".$userId;
			
			mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		}
	}
	else if($action == "delete")
	{
		$strIuQuery = "DELETE FROM Users Where UserId = ".$userId;
		mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		header("location: user.php");
		exit();
	}

	//echo $strIuQuery;
	
	// Main Select Query
	$strQuery = "SELECT * FROM Users WHERE UserId = ". $userId;
	
	$result = mysql_query($strQuery) or die($strQuery."<br/><br/>".mysql_error());
	$row = mysql_fetch_array($result);
	
	$initials = $row["Initials"];
	$firstName = $row["FirstName"];
	$lastName = $row["LastName"];
	$address = $row["Address"];
	$phoneNumber = $row["PhoneNumber"];
	$mobileNumber = $row["MobileNumber"];
	$gender = $row["Gender"];
	$email = $row["Email"];
	$salary = $row["SalaryPerMonth"];
	$dob = $row["DateOfBirth"];
	$isActive = $row["IsActiveUser"];
	$userName = $row["UserName"];
	$userType = $row["UserTypeId"];
	$password = $row["Password"];
	
	if($isActive == "0")
		$checked = "";
	if($gender == "Female")
		$chkFemale="checked";
	else
		$chkMale = "checked";
	if($userType == "1")
		$selAdmin = "selected";
	elseif($userType == "2")
		$selMod = "selected";
	else
		$selStaff = "selected";	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Users Detail</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="css/mainStyle.css" />
	<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/jquery.totemticker.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#dob").datepicker({
				inline: true,
				dateFormat: 'yy-mm-dd'
			});
		});
		
		var isRequired = true;
		$(document).ready(function() {
			ShowBulletin(20);
			Ticker();
			var selectedUserType = "<?php echo $userType ?>";
			if(selectedUserType == "1" || selectedUserType == "2")
			{
				isRequired = true;
				$('.showHide').show();
			}
			
			$('#selectUserType').change(function() {
				if($('#selectUserType').val() == "1" || $('#selectUserType').val() == "2")
				{	
					isRequired = true;
					$('.showHide').show();
				}
				else
				{
					$('#txtUserName').val('');
					$('#txtPassword').val('');
					isRequired = false;
					$('.showHide').hide();
				}
			});
		});
		
		function fnSave()
		{
			$("#form1").validate({
				rules: {
					txtFirstName: "required",
					txtLastName: "required",
					txtUserName: {
						required: isRequired,
						minlength: 4
						},
					txtPassword: {
						required: false,
						minlength: 7
					},
					txtEmail: {
						required: false,
						email: true
					},
					txtAddress: "required",
					txtMobNumber: {
						required: true,
						number:true,	
						minlength: 10
					}
				},
				messages: {
					txtFirstName: "Please enter First Name",
					txtLastName: "Please enter Last Name",
					txtUserName: {
						required: "Please enter a User Name",
						minlength: "User Name must be at least 4 characters long"
					},
					txtPassword: {
						minlength: "Password must be at least 7 characters long"
					},
					txtEmail: "Please enter a valid email address",
					txtAddress: "Please enter full Address",
					txtMobNumber: {
						required: "Please enter Mobile Number",
						minlength: "Mobile Number must be at least of 10 numbers"
					}
				}
			});
			
			fnAction('save');
		}
	</script>
</head>
<body>
<div id="main">
	<?php require_once('navigation.php')?>
	<div id="middle">
		<div id="left-column">
			<h3>Bulletin Ticker</h3>
			<ul id="vertical-ticker">
				<div id="divBulletin">
				</div>
			</ul>
			<h3><a href="#" id="ticker-previous">Previous</a> &nbsp;&nbsp;&nbsp;<a href="#" id="ticker-next">Next</a></h3>
		</div>
		<div id="center-column">
			<div class="top-bar">
				<h1>Users</h1>
				<div class="breadcrumbs"><a href="#">Users & Staffs</a> / <a href="#">Detail Page</a></div>
			</div><br />
			<form id="form1" action="" method="post">
				<div class="select-bar-top">
					<input type="hidden" name="action" id="action" />
					<a href="javascript:window.location.href='user.php';" title="Back To List" class="back" />Back</a>
					<a href="userDetail.php?userId=<?php echo $userId ?>" title="Reload" class="refresh" />Refresh</a>
					<a href="javascript:fnSave();" title="Save" class="save" />Save</a>
					<a href="javascript:fnAction('delete');" title="Delete" class="delete" />Delete</a>
					<a href="javascript:fnDetail('userDetail.php','userId=0');" title="Add New" class="addNew" />Add</a>
				</div>
				<div class="table">
					<img src="images/bg-th-left.gif" width="8" height="7" alt="" class="left" />
					<img src="images/bg-th-right.gif" width="7" height="7" alt="" class="right" />
					<table class="listing form" cellpadding="0" cellspacing="0">
						<tr>
							<th class="full" colspan="2">User Details</th>
						</tr>
						<tr>
							<td class="first"><strong>Initials</strong></td>
							<td class="last"><input type="text" style="width:100px;" maxlength="7" name="txtInitials" class="text" value="<?php echo $initials ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>First Name</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtFirstName" id="txtInitials" class="text" value="<?php echo $firstName ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Last Name</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtLastName" id="txtLastName" class="text" value="<?php echo $lastName ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Date Of Birth</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtDob" class="text" id="dob" value="<?php echo $dob ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Address</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtAddress" id="txtAddress" class="text" value="<?php echo $address ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Gender</strong></td>
							<td class="last">
								<input type="radio" name="radGender" <?php echo $chkMale ?> value="Male" /> Male
								<input type="radio" name="radGender" <?php echo $chkFemale ?> value="Female" /> Female
							</td>
						</tr>
						<tr>
							<td class="first"><strong>Email</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtEmail" id="txtEmail" class="text" value="<?php echo $email ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Phone Number</strong></td>
							<td class="last"><input type="text" maxlength="15" name="txtPhnNumber" id="txtPhnNumber" class="text" value="<?php echo $phoneNumber ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Mobile Number</strong></td>
							<td class="last"><input type="text" maxlength="15" name="txtMobNumber" id="txtMobNumber" class="text" value="<?php echo $mobileNumber ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Is Active</strong></td>
							<td class="last"><input type="checkbox" name="chkIsActive" <?php echo $checked ?> /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>User Type</strong></td>
							<td class="last">
								<select id="selectUserType" name="selectUserType" class="dropDown">
									<option value="1" <?php echo $selAdmin ?>>Administrator</option>
									<option value="2" <?php echo $selMod ?>>Moderator</option>
									<option value="3" <?php echo $selStaff ?>>Employee/Staff</option>
								</select>
							</td>
						</tr>
						<tr class="showHide">
							<td class="first"><strong>User Name</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtUserName" id="txtUserName" class="text" value="<?php echo $userName ?>" /></td>
						</tr>
						<tr class="showHide">
							<td class="first"><strong>Password</strong></td>
							<td class="last"><input type="password" maxlength="30" name="txtPassword" id="txtPassword" class="showHideText" value=""/></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Salary Per Month</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtSpm" class="text" value="<?php echo $salary ?>" /></td>
						</tr>
					</table>
					<p>&nbsp;</p>
				</div>
			</form>
		</div>
		<div id="right-column">
			<strong class="h">INFO</strong>
			<div class="box">Detail page for the user. Here you can add/edit/delete certain user. You can also assign administrator/moderator for any staff but it requires assigning User Name and Password too.</div>
	  </div>
	</div>
	<div id="footer">
		IMS 2012 &copy; Developed By Er.Sajish Shilpakar.
		Follow me at: <a href="http://www.sajish.com.np" target="blank">www.sajish.com.np</a>
		<a href="http://www.facebook.com/sajish" target="blank" title="Sajish Facebook" class="fb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>


</body>
</html>


