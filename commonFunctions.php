<?php
	function CheckSession($chk)
	{
		if(!isset($_SESSION['UserId']) || (trim($_SESSION['UserId']) == '')){
			header("location: index.php");
			exit();
		}
		else
		{
			if($chk == 1)
			{
				if($_SESSION['UserType'] != "1")
				{	
					header("location: index.php");
					exit();
				}
			}
		}
	}
	
	function Sanitize($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	function GetUserType($userTypeId)
	{
		$retval = "";
		if($userTypeId == "1")
		{
			$retval = "Administrator";
		}
		if($userTypeId == "2")
		{
			$retval = "Moderator";
		}
		if($userTypeId == "3")
		{
			$retval = "Staff";
		}
		return $retval;
	}
	
	function GetPaging($pages, $page)
	{
		$retval = "<select name='selectPage' id='selectPage' onChange='fnNavigate()'>";
		for($i = 1; $i <= $pages; $i++) {
			if($i == $page)
			{
				$retval .= "<option value='".$i."' selected>".$i."</option>";
			}
			else
			{
				$retval .= "<option value='".$i."'>".$i."</option>";
			}
		}
		$retval .= "</select>";
		return $retval;
	}
?>