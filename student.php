<?php
	//error_reporting(0); 
	session_start();
	require_once('connection.php');
	require_once('commonFunctions.php');
	CheckSession(0);
	$thisPage = "Student";
	$searchField = "";
	$strWhere = "";
	
	// Main Select Query
	$strQuery = "SELECT StudentId,FullName,Address,MobileNumber,Name,Purpose FROM Students S INNER JOIN Courses C ON S.CourseId = C.CourseId WHERE 1=1";
	
	// Handling Search Filters
	if(isset($_POST["txtSearch"]))
	{
		$searchField = Sanitize($_POST["txtSearch"]);
	}	
	if($searchField != "")
	{
		$strWhere = $strWhere." AND FullName LIKE '%".$searchField."%'";
	}
	
	// Pagination Part
	$recordsPerPage = 10;
	$page = isset($_POST['page']) ? (int) $_POST['page'] : 1;
	$pages = implode(mysql_fetch_assoc(mysql_query("SELECT COUNT(*) FROM Students WHERE 1=1".$strWhere)));
	$pages = ceil($pages / $recordsPerPage);
	$strQuery = $strQuery.$strWhere." ORDER BY FullName ASC LIMIT " . (($page - 1) * $recordsPerPage) . ", ".$recordsPerPage;
	
	// Fetching Data To List
	$result = mysql_query($strQuery) or die($strQuery."<br/><br/>".mysql_error());
	$totalCount = mysql_num_rows($result);	
	$i = 0;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Student</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="css/mainStyle.css" />
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.totemticker.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			ShowBulletin(20);
			Ticker();
			$("#btnSubmit").click(function() {
				$("#page").val(1);
				$("#form1").submit();      
			});
			
			fnRowHover();
		});
	</script>
</head>
<body>
<div id="main">
	<?php require_once('navigation.php')?>
	<div id="middle">
		<div id="left-column">
			<h3>Bulletin Ticker</h3>
			<ul id="vertical-ticker">
				<div id="divBulletin">
				</div>
			</ul>
			<h3><a href="#" id="ticker-previous">Previous</a> &nbsp;&nbsp;&nbsp;<a href="#" id="ticker-next">Next</a></h3>
		</div>
		<div id="center-column">
			<div class="top-bar">
				<h1>Students</h1>
				<div class="breadcrumbs"><a href="#">Student</a> / <a href="#">List Page</a></div>
			</div><br />
			<div class="select-bar">
				<form id="form1" action="#" method="post">
					<input type="hidden" name="page" id="page" />
					<label>
						Search By Name: <input type="text" name="txtSearch" value="<?php echo $searchField; ?>"/>
					</label>
					<label>
						<input type="button" id="btnSubmit" class="searchButton" value="Search" />
					</label>
					<a href="student.php" title="Reload" class="refresh" />Refresh</a>
					<a href="javascript:fnDetail('studentDetail.php','studentId=0');" title="Add New" class="addNew"/>Add</a>	
				</form>
			</div>
			<div class="table">
				<img src="images/bg-th-left.gif" width="8" height="7" alt="" class="left" />
				<img src="images/bg-th-right.gif" width="7" height="7" alt="" class="right" />
				<table class="listing-list" cellpadding="0" cellspacing="0">
					<tr>
						<th class="firstX">Full Name</th>
						<th class="first">Address</th>
						<th class="first">Mobile</th>
						<th class="first">Course</th>
						<th class="first">Purpose</th>
					</tr>
					<?php if($totalCount == 0){ ?>
						<tr>
							<td colspan="6">
								Sorry, There are no any data.
							</td>
						</tr>
					<?php } else { ?>
						<?php while($row = mysql_fetch_array($result)){ ?>
							<tr <?php echo $i++ % 2 ? 'class="bg"' : ''; ?> onClick="fnDetail('studentDetail.php','studentId=<?php echo $row['StudentId']?>')">
								<td><?php echo $row['FullName']?></td>
								<td><?php echo $row['Address']?></td>
								<td><?php echo $row['MobileNumber']?></td>
								<td><?php echo $row['Name']?></td>
								<td><?php echo $row['Purpose'] == 'A' ? 'Admission' : 'Visit';?></td>
							</tr>
						<?php } ?>
					<?php } ?>
				</table>
				<div class="select">
					<strong>Page : </strong>
					<?php
						echo GetPaging($pages, $page)
					 ?> 
				</div>
			</div>
		</div>
		<div id="right-column">
			<strong class="h">INFO</strong>
			<div class="box">List page for the students. You can filter according to search criteria. Inorder to see details and edit, click on desired row.</div>
	  </div>
	</div>
	<div id="footer">
		IMS 2012 &copy; Developed By Er.Sajish Shilpakar.
		Follow me at: <a href="http://www.sajish.com.np" target="blank">www.sajish.com.np</a>
		<a href="http://www.facebook.com/sajish" target="blank" title="Sajish Facebook" class="fb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>


</body>
</html>


