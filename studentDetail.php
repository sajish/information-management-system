<?php
	//error_reporting(0); 
	session_start();
	require_once('connection.php');
	require_once('commonFunctions.php');
	CheckSession(0);
	$thisPage = "Student";
	$strIuQuery="";
	$chkMale="";
	$chkFemale="";
	$selA="";
	$selV="";
		
	$studentId = isset($_GET["studentId"]) ? Sanitize($_GET["studentId"]) : '0'; 
	$action = isset($_POST['action']) ? Sanitize($_POST['action']) : ''; 
	$reqName = isset($_POST['txtFullName']) ? Sanitize($_POST['txtFullName']) : ''; 
	$reqAddress = isset($_POST['txtAddress']) ? Sanitize($_POST['txtAddress']) : ''; 
	$reqAge = isset($_POST['txtAge']) ? Sanitize($_POST['txtAge']) : ''; 
	$reqGender = isset($_POST['radGender']) ? Sanitize($_POST['radGender']) : ''; 
	$reqMob = isset($_POST['txtMobile']) ? Sanitize($_POST['txtMobile']) : ''; 
	$reqPhone = isset($_POST['txtPhone']) ? Sanitize($_POST['txtPhone']) : ''; 
	$reqEmail = isset($_POST['txtEmail']) ? Sanitize($_POST['txtEmail']) : ''; 
	$reqCourse = isset($_POST['txtCourse']) ? Sanitize($_POST['txtCourse']) : ''; 
	$reqPurpose = isset($_POST['txtPurpose']) ? Sanitize($_POST['txtPurpose']) : ''; 
	$reqPaid = isset($_POST['txtPaidAmount']) ? Sanitize($_POST['txtPaidAmount']) : ''; 
	$reqDiscount = isset($_POST['txtDiscount']) ? Sanitize($_POST['txtDiscount']) : '';
	$reqComment = isset($_POST['txtComment']) ? Sanitize($_POST['txtComment']) : '';
	
	if($reqCourse != '')
	{
		$splittedCourse = explode("#$#", $reqCourse);
		$reqCourse = Sanitize($splittedCourse[0]);
	}
	
	if($action == "save")
	{
		if($studentId == "0")
		{
			$strIuQuery = "INSERT INTO Students(FullName,Age,Gender,Address,MobileNumber,PhoneNumber,Email,CourseId,Purpose,PaidAmount,Discount,Comments)";
			$strIuQuery .= " VALUES('".$reqName."','".$reqAge."','".$reqGender."','".$reqAddress."','".$reqMob."','".$reqPhone."','".$reqEmail."','".$reqCourse."','".$reqPurpose."','".$reqPaid."','".$reqDiscount."','".$reqComment."')";
			
			mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
			$studentId = mysql_insert_id();
			header("location: studentDetail.php?studentId=".$studentId);
			exit();
		}
		else
		{
			$strIuQuery = "UPDATE Students SET FullName='".$reqName."',Age='".$reqAge."',Gender='".$reqGender."',Address='".$reqAddress."',PhoneNumber='".$reqPhone."',";
			$strIuQuery .= "MobileNumber='".$reqMob."',Email='".$reqEmail."',CourseId='".$reqCourse."',Purpose='".$reqPurpose."',";
			$strIuQuery .= "PaidAmount='".$reqPaid."',Discount='".$reqDiscount."',Comments='".$reqComment."'";
			$strIuQuery .= " WHERE StudentId = ".$studentId;
			mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		}
	}
	else if($action == "delete")
	{
		$strIuQuery = "DELETE FROM Students WHERE StudentId = ".$studentId;
		mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		header("location: student.php");
		exit();
	}
	
	// Main Select Query
	$strQuery = "SELECT *, S.CourseId AS MainCourseId FROM Students S INNER JOIN Courses C ON S.CourseId = C.CourseId WHERE StudentId = ".$studentId;
	
	$result = mysql_query($strQuery) or die($strQuery."<br/><br/>".mysql_error());
	$row = mysql_fetch_array($result);
	
	$gender = $row["Gender"];
	$courseId = $row["MainCourseId"];
	$purpose = $row["Purpose"];
	
	if($gender == "F")
		$chkFemale="checked";
	else
		$chkMale = "checked";
		
	if($purpose == "A")
		$selA="selected";
	else
		$selV = "selected";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Student Detail</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="css/mainStyle.css" />	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/jquery.totemticker.js"></script>
	<script type="text/javascript">
		function fnSave()
		{
			$("#form1").validate({
				rules: {
					txtFullName: "required",
					txtAddress: "required",
					txtAge: {
						required: false,
						number:true
					},
					txtMobile: {
						required: false,
						number:true,	
						minlength: 10
					},
					txtEmail: {
						required: false,
						email: true
					},
					txtPaidAmount: {
						required: false,
						number: true
					},
					txtDiscount: {
						required: false,
						number: true,
						max: 100
					},
					txtPhone: {
						required: false,
						number:true,	
						minlength: 7
					}
				},
				messages: {
					txtFullName: "Please enter Full Name",
					txtAddress: "Please enter full Address",
					txtMobile: {
						minlength: "Mobile Number must be at least of 10 numbers"
					},
					txtEmail: "Please enter a valid email address",
					txtPhone: {
						minlength: "Mobile Number must be at least of 7 numbers"
					}
				}
			});
			
			fnAction('save');
		}
		
		$(document).ready(function() {
			ShowBulletin(20);
			Ticker();
			fnCalculate();
			var purpose = "<?php echo $purpose ?>";
			if(purpose == "A")
			{
				isRequired = true;
				$('.showHide').show();
			}
			
			$('#txtPurpose').change(function() {
				if($('#txtPurpose').val() == "A")
				{
					$('.showHide').show();
				}
				else
				{
					$('#txtDiscount').val('');
					$('#txtPaidAmount').val('');
					$('.showHide').hide();
				}
			});
			
			var str = $('#txtCourse').val();
			var subStr = str.split('#$#');
			$('#price').html("Rs. " + subStr[1]);
			
			$('#txtCourse').change(function() {
				var str = $('#txtCourse').val();
				var subStr = str.split('#$#');
				$('#price').html("Rs. " + subStr[1]);
				fnCalculate();
			});
		});
		
		function fnCalculate()
		{
			var discount = $('#txtDiscount').val();
			var totalPrice = $('#price').html();
			var paidPrice = $('#txtPaidAmount').val();
			
			var dicountedPrice = (discount / 100) * totalPrice.substring(3);
			var remainingPrice = totalPrice.substring(3) - paidPrice;
			var finalAmount = remainingPrice - dicountedPrice;
			
			$('#remainingAmount').html("Rs. " + finalAmount);
		}
		
	</script>
</head>
<body>
<div id="main">
	<?php require_once('navigation.php')?>
	<div id="middle">
		<div id="left-column">
			<h3>Bulletin Ticker</h3>
			<ul id="vertical-ticker">
				<div id="divBulletin">
				</div>
			</ul>
			<h3><a href="#" id="ticker-previous">Previous</a> &nbsp;&nbsp;&nbsp;<a href="#" id="ticker-next">Next</a></h3>
		</div>
		<div id="center-column">
			<div class="top-bar">
				<h1>Students</h1>
				<div class="breadcrumbs"><a href="#">Student</a> / <a href="#">Detail Page</a></div>
			</div><br />
			<form id="form1" action="" method="post">
				<div class="select-bar-top">
					<input type="hidden" name="action" id="action" />
					<a href="javascript:window.location.href='student.php';" title="Back To List" class="back" />Back</a>
					<a href="studentDetail.php?studentId=<?php echo $studentId ?>" title="Reload" class="refresh" />Refresh</a>
					<a href="javascript:fnSave();" title="Save" class="save" />Save</a>
					<a href="javascript:fnAction('delete');" title="Delete" class="delete" />Delete</a>
					<a href="javascript:fnDetail('studentDetail.php','studentId=0');" title="Add New" class="addNew" />Add</a>
				</div>
				<div class="table">
					<img src="images/bg-th-left.gif" width="8" height="7" alt="" class="left" />
					<img src="images/bg-th-right.gif" width="7" height="7" alt="" class="right" />
					<table class="listing form" cellpadding="0" cellspacing="0">
						<tr>
							<th class="full" colspan="2">Student Details</th>
						</tr>
						<tr>
							<td class="first"><strong>Full Name</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtFullName" class="text" value="<?php echo $row["FullName"] ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Age</strong></td>
							<td class="last"><input type="text" maxlength="4" name="txtAge" id="txtAge" class="text" value="<?php echo $row["Age"] ?>" /></td>
						</tr>
						<tr  class="bg">
							<td class="first"><strong>Gender</strong></td>
							<td class="last">
								<input type="radio" name="radGender" <?php echo $chkMale ?> value="M" /> Male
								<input type="radio" name="radGender" <?php echo $chkFemale ?> value="F" /> Female
							</td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Address</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtAddress" id="txtAddress" class="text" value="<?php echo $row["Address"] ?>" /></td>
						</tr>
						<tr>
							<td class="first"><strong>Mobile Number</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtMobile" id="txtMobile" class="text" value="<?php echo $row["MobileNumber"] ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Phone Number</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtPhone" id="txtPhone" class="text" value="<?php echo $row["PhoneNumber"] ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Email</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtEmail" id="txtEmail" class="text" value="<?php echo $row["Email"] ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Course</strong></td>
							<td class="last">
								<select name="txtCourse" id="txtCourse" class="dropDown">
									<?php
										$strSelCourse = "SELECT CONCAT_WS('#$#', CourseId, Price) FullValue,CourseId,Name FROM Courses ORDER BY Name ASC";
										$resultCourse = mysql_query($strSelCourse) or die($strSelCourse."<br/><br/>".mysql_error());
										
										while($rowCourse = mysql_fetch_array($resultCourse)){
											if($courseId == Sanitize($rowCourse["CourseId"]))
											{
												echo "<option value='".$rowCourse["FullValue"]."' selected>".$rowCourse["Name"]."</option>";
											}
											else
											{
												echo "<option value='".$rowCourse["FullValue"]."'>".$rowCourse["Name"]."</option>";
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Course Amount</strong></td>
							<td class="last"><span id="price" style="margin-left:50px;">Rs. <?php echo $row["Price"] ?></span></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Purpose</strong></td>
							<td class="last">
								<select id="txtPurpose" name="txtPurpose" class="dropDown">
									<option value="A" <?php echo $selA ?>>Admission</option>
									<option value="V" <?php echo $selV ?>>General / Visit</option>
								</select>
							</td>
						</tr>
						<tr class="showHide">
							<td class="first"><strong>Discount %</strong></td>
							<td class="last"><input type="text" maxlength="3" name="txtDiscount" id="txtDiscount" onBlur="fnCalculate()" class="text" value="<?php echo $row["Discount"] ?>" /></td>
						</tr>
						<tr class="showHide">
							<td class="first"><strong>Paid Amount</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtPaidAmount" id="txtPaidAmount" onBlur="fnCalculate()" class="text" value="<?php echo $row["PaidAmount"] ?>" /></td>
						</tr>
						<tr class="showHide">
							<td class="first"><strong>Due Amount</strong></td>
							<td class="last"><span id="remainingAmount" style="margin-left:50px;"></span></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Comments</strong></td>
							<td class="last"><textarea maxlength="200" rows="4" name="txtComment" id="txtComment" class="textArea" /><?php echo $row["Comments"] ?></textarea></td>
						</tr>
					</table>
					<p>&nbsp;</p>
				</div>
			</form>
		</div>
		<div id="right-column">
			<strong class="h">INFO</strong>
			<div class="box">Detail page for the student. Here you can add/edit/delete student. You can also assign visit/admission. Also money due and paid are calculated.</div>
	  </div>
	</div>
	<div id="footer">
		IMS 2012 &copy; Developed By Er.Sajish Shilpakar.
		Follow me at: <a href="http://www.sajish.com.np" target="blank">www.sajish.com.np</a>
		<a href="http://www.facebook.com/sajish" target="blank" title="Sajish Facebook" class="fb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>
</body>
</html>


