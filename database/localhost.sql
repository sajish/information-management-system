-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2012 at 03:51 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ims`
--
CREATE DATABASE `ims` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ims`;

-- --------------------------------------------------------

--
-- Table structure for table `bulletins`
--

CREATE TABLE IF NOT EXISTS `bulletins` (
  `BulletinId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(30) NOT NULL,
  `Message` varchar(200) NOT NULL,
  `User` varchar(50) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`BulletinId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `bulletins`
--

INSERT INTO `bulletins` (`BulletinId`, `Title`, `Message`, `User`, `Date`) VALUES
(1, 'Important Notice', 'Our Office remains close on coming Monday on the occasion of Shiva Ratri 2068\r\nThank You', 'Sajish Shilpakar', '2012-01-31 17:55:25'),
(2, 'Notice By Staff', 'Our salary Has not been deposited in our accout. Please Check', 'Sajish Shilpakar', '2012-01-31 17:56:04'),
(3, 'Welcome', 'A Warm Welcome to IMS Portal. You can enjoy now', 'Sajish Shilpakar', '2012-01-31 17:56:51'),
(4, 'Hi', 'Hi I am bidur', 'Bidur Chaulagain', '2012-01-31 18:50:38'),
(5, 'lawan', 'this is test', 'Sajish Shilpakar', '2012-02-01 14:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `CourseId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Price` decimal(10,0) NOT NULL,
  PRIMARY KEY (`CourseId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`CourseId`, `Name`, `Description`, `Price`) VALUES
(1, 'Basic Course', 'Advance Basic Course Part I', '5000'),
(3, 'Advance Basic Course', 'Advance Basic Course Part II', '700');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `StudentId` int(11) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(50) NOT NULL,
  `Age` int(11) DEFAULT NULL,
  `Gender` varchar(1) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `MobileNumber` varchar(30) DEFAULT NULL,
  `PhoneNumber` varchar(30) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `CourseId` int(11) NOT NULL,
  `Purpose` varchar(1) NOT NULL,
  `PaidAmount` decimal(10,0) NOT NULL,
  `Discount` int(11) NOT NULL,
  `Comments` varchar(200) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`StudentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`StudentId`, `FullName`, `Age`, `Gender`, `Address`, `MobileNumber`, `PhoneNumber`, `Email`, `CourseId`, `Purpose`, `PaidAmount`, `Discount`, `Comments`, `Date`) VALUES
(1, 'Shyam Shrestha', 19, 'M', 'Kathmandu, Nepal', '+9779841604563', '1234', 'ram@bam.com', 1, 'V', '0', 0, '', '2012-02-06 18:15:00'),
(2, 'Rama Shrestha', 19, 'F', 'Kathmandu, Nepal', '+9779841604563', '12345678', 'ram@bam.com', 3, 'A', '0', 0, 'this is test''s // \\\\ '' OR 1= 1', '2012-02-06 18:15:00'),
(3, 'gum Shrestha', 19, 'M', 'Kathmandu, Nepal', '9779841604563', '12345678', 'ram@bam.com', 3, 'V', '0', 0, '', '2012-02-06 18:15:00'),
(4, 'man Shrestha', 19, 'F', 'Kathmandu, Nepal', '9779841604563', '12345678', 'ram@bam.com', 1, 'A', '10', 10, '', '2012-02-06 18:15:00'),
(5, 'test', 13, 'F', 'bhkt 12', '30233213213', '', '', 3, 'A', '50', 10, 'test', '2012-02-06 18:15:00'),
(6, 'hari', 10, 'M', 'tika', '1234678901', '1312321321', 'asd@asd.coasd', 1, 'A', '100', 0, 'sadaaaaaaaaa,;saldlsakdsalkdlsadlksa;kdksads', '2012-02-06 18:15:00'),
(7, 'Kumar', 12, 'M', 'katham', '', '', '', 3, 'V', '0', 0, '', '2012-02-06 18:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(30) NOT NULL,
  `Initials` varchar(15) DEFAULT NULL,
  `FirstName` varchar(30) NOT NULL,
  `LastName` varchar(30) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `PhoneNumber` varchar(20) DEFAULT NULL,
  `MobileNumber` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `UserTypeId` int(11) NOT NULL,
  `IsActiveUser` varchar(1) DEFAULT NULL,
  `Password` varchar(100) NOT NULL,
  `Gender` varchar(10) DEFAULT NULL,
  `SalaryPerMonth` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserId`, `UserName`, `Initials`, `FirstName`, `LastName`, `Address`, `DateOfBirth`, `PhoneNumber`, `MobileNumber`, `Email`, `UserTypeId`, `IsActiveUser`, `Password`, `Gender`, `SalaryPerMonth`) VALUES
(1, 'admin', 'Mr', 'Sajish', 'Shilpakar', 'Bhaktapur - 13, Nepal', '1988-09-03', '01-6610731', '+9779841604423', 'sajishshilpakar@gmail.com', 1, '1', 'b7f5113c7c1c69de92d70c1164283e74 ', 'Male', '0'),
(2, 'test', 'Mr', 'Bidur', 'Chaulagain', 'kathmaaaaaa', '0000-00-00', '', '12345678890', '', 2, '1', 'b7f5113c7c1c69de92d70c1164283e74', 'Male', '0'),
(5, 'test5', NULL, 'sabin', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '100'),
(6, 'test6', NULL, 'nabin', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '100'),
(8, '', 'Mr', 'Man Bahadur', 'takia', 'naso', '2012-01-03', '', '921312321321', '', 3, '1', '', 'Male', '1344'),
(9, 'rertetTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(10, 'opoTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(11, 'gdhfhgTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '0', '', NULL, '13244'),
(12, 'muymTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '0', '', NULL, '13244'),
(13, 'hrthhTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(14, 'ewqewqeTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(15, 'qweTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '0', '', NULL, '13244'),
(16, 'sadadTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(17, 'daTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '0', '', NULL, '13244'),
(18, 'sssTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(19, 'dsadTock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', '', NULL, '13244'),
(21, 'Todsadck', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '0', '', NULL, '13244'),
(22, 'Tsaock', '', 'TTan', NULL, NULL, NULL, NULL, NULL, NULL, 3, '0', '', NULL, '13244'),
(28, '', 'test', 'xyz', '', '', '0000-00-00', '', '', '', 0, '1', '', 'Male', '0'),
(29, '', 'n', 'as', '', '', '0000-00-00', '', '', '', 0, '0', '', 'Male', '0'),
(34, '', '', 'saj', 'man', '', '0000-00-00', '', '', '', 0, '0', '', 'Male', '0'),
(35, '', '', 'amans', 'dahal', '', '0000-00-00', '', '', '', 3, '0', '03c7c0ace395d80182db07ae2c30f034', 'Male', '0'),
(36, '', '', 's', 's', '', '0000-00-00', '', '', '', 1, '0', '', 'Male', '0'),
(37, '', '', 'sex', 's', '', '0000-00-00', '', '', '', 3, '0', '', 'Male', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
