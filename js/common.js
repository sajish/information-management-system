// jQuery Input Hints plugin
// Copyright (c) 2009 Rob Volk
// http://www.robvolk.com
jQuery.fn.inputHints=function() {
    // hides the input display text stored in the title on focus
    // and sets it on blur if the user hasn't changed it.

    // show the display text
    $(this).each(function(i) {
        $(this).val($(this).attr('title'))
            .addClass('hint');
    });

    // hook up the blur & focus
    return $(this).focus(function() {
        if ($(this).val() == $(this).attr('title'))
            $(this).val('')
                .removeClass('hint');
    }).blur(function() {
        if ($(this).val() == '')
            $(this).val($(this).attr('title'))
                .addClass('hint');
    });
};

function fnRowHover()
{
	$('table tbody tr').mouseover(function() {
		$(this).addClass('selectedRow');
	}).mouseout(function() {
		$(this).removeClass('selectedRow');
	});
}

function fnNavigate(){
	$("#page").val($("#selectPage").val());
	$("#form1").submit();
}

function fnDetail(page,id)
{
	window.location.href = page + "?" + id;
}

function fnAction(actionType)
{
	$("#action").val(actionType);
	$("#form1").submit();
}

function ShowBulletin(numberOf)
{
	if (numberOf == "")
	{
		document.getElementById("divBulletin").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{	// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{	// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			document.getElementById("divBulletin").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","bulletinAjax.php?results="+numberOf,true);
	xmlhttp.send();
}

function Ticker()
{
	$('#vertical-ticker').totemticker({
		row_height	:	'200px',
		interval	:	4000,
		speed		:	1500,
		mousestop	:	true,
		next		:	'#ticker-next',
		previous	:	'#ticker-previous',
	});
}