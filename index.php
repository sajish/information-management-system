<?php
	
	require_once('connection.php');
	require_once('commonFunctions.php');
	
	$userName = "";
	$password = "";
	$msg = "";
	
	if(isset($_POST["txtUserName"]))
	{
		$userName = Sanitize($_POST["txtUserName"]);
	}
	if(isset($_POST["txtPassword"]))
	{
		$password = Sanitize($_POST['txtPassword']);
		$password = md5($password);
	}
	
	if($userName != "" && $password != "")
	{
		$strQuery = "SELECT UserId, UserName, UserTypeId, Password, CONCAT_WS(' ', FirstName, LastName) FullName FROM Users Where IsActiveUser = 1 AND UserName = '".$userName."'";
		$result = mysql_query($strQuery);
		
		if (mysql_num_rows($result) == 1)
		{
			$row = mysql_fetch_row($result);
			if (trim($row[3]) == $password)
			{
				session_start();
				session_regenerate_id();
				$_SESSION['UserId'] = $row[0];
				$_SESSION['UserName'] = $row[1];
				$_SESSION['UserType'] = $row[2];
				$_SESSION['FullName'] = $row[4];
				session_write_close();
				header("location: home.php");
				exit();
			}
			else
			{
				$msg = "Password did not match.";
			}
		}
		else
		{
			$msg = "We have no such user.";
		}
	}	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>IMS Login Portal</title>
	<link rel="stylesheet" type="text/css" href="css/loginStyle.css" />
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			//$('input[title]').inputHints();
			$("#txtUserName").focus();
			$("#form1").validate({
				rules: {
					txtUserName: "required",
					txtPassword: "required"
				},
				messages: {
					txtUserName: "Please enter Username",
					txtPassword: "Please enter Password"
				}
			});
			$("#txtUserName").blur(function() {
				$(".err").html('');
			});
		});
	</script>
</head>

<body>
	<form id="form1" action="#" method="post">
		<fieldset>
			<legend>Log in</legend>
			
			<label for="userName">Log In As</label>
			<input type="text" id="txtUserName" name="txtUserName" maxlength="30"/>
			<div class="clear"></div>
			
			<label for="password">Password</label>
			<input type="password" id="txtPassword" name="txtPassword" maxlength="30"/>
			<div class="clear"></div><br /><br />
			
			<input type="submit" id="btnLogin" style="margin: -20px 0 0 287px;" class="button" name="submit" value="Log In"/>	
		</fieldset>
		<span class="err"><?php echo $msg ?></span>
	</form>
</body>

</html>