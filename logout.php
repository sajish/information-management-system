<?php
session_start();
$_SESSION['UserId'] = "";

session_destroy();
session_regenerate_id();
session_write_close();
header("location: index.php");
exit();
?>