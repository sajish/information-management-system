<?php 
	session_start();
	require_once('commonFunctions.php');
	CheckSession(0);
	$thisPage = "Home";
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Home</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="css/mainStyle.css" />
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.totemticker.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript">		
		$(document).ready(function() {
			ShowBulletin(20);
			Ticker();
		});
	</script>
</head>
<body>
<div id="main">
	<?php require_once('navigation.php')?>
	<div id="middle">
		<div id="left-column">
			<h3>Bulletin Ticker</h3>
			<ul id="vertical-ticker">
				<div id="divBulletin">
				</div>
			</ul>
			<h3><a href="#" id="ticker-previous">Previous</a> &nbsp;&nbsp;&nbsp;<a href="#" id="ticker-next">Next</a></h3>
		</div>
		<div id="center-column">
			<div class="top-bar">
				<h1>Home</h1>
				<div class="breadcrumbs"><a href="#">Home</a> / <a href="#">Contents</a></div>
			</div><br />
		  <div class="select-bar"></div>
		  Welcome to IMS Portal. This is a simple web application where you can manage information of the company.
		</div>
		<div id="right-column">
			<strong class="h">INFO</strong>
			<div class="box">This is just a welcome home page. On the left side there is ticker where you can see current message on bulletion board.</div>
		</div>
	</div>
	<div id="footer">
		IMS 2012 &copy; Developed By Er.Sajish Shilpakar.
		Follow me at: <a href="http://www.sajish.com.np" target="blank">www.sajish.com.np</a>
		<a href="http://www.facebook.com/sajish" target="blank" title="Sajish Facebook" class="fb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>


</body>
</html>


