<?php
	//error_reporting(0); 
	session_start();
	require_once('connection.php');
	require_once('commonFunctions.php');
	CheckSession(0);
	$thisPage = "Bulletin";
	$strIuQuery="";
	$afterMessage = "";
	
	$action = isset($_POST['action']) ? Sanitize($_POST['action']) : ''; 
	$reqTitle = isset($_POST['txtTitle']) ? Sanitize($_POST['txtTitle']) : ''; 
	$reqMessage = isset($_POST['txtMessage']) ? Sanitize($_POST['txtMessage']) : ''; 
	$reqUser = $_SESSION['FullName'];
	
	if($action == "save")
	{
		$strIuQuery = "INSERT INTO Bulletins(Title,Message,User)";
		$strIuQuery .= " VALUES('".$reqTitle."','".$reqMessage."','".$reqUser."')";
		
		mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		$afterMessage = "Dear ".$reqUser.". Your message has been successfully posted in our bulletin board.";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Bulletin Board</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="css/mainStyle.css" />	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.totemticker.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript">
		function fnSave()
		{
			$("#form1").validate({
				rules: {
					txtTitle: "required",
					txtMessage: "required"
				},
				messages: {
					txtTitle: "Please enter Title",
					txtMessage: "Please enter Message"
				}
			});
			
			fnAction('save');
		}
		
		$(document).ready(function() {
			ShowBulletin(20);
			Ticker();
		});
	</script>
</head>
<body>
<div id="main">
	<?php require_once('navigation.php')?>
	<div id="middle">
		<div id="left-column">
			<h3>Bulletin Ticker</h3>
			<ul id="vertical-ticker">
				<div id="divBulletin">
				
				</div>
			</ul>
			<h3><a href="#" id="ticker-previous">Previous</a> &nbsp;&nbsp;&nbsp;<a href="#" id="ticker-next">Next</a></h3>
		</div>
		<div id="center-column">
			<div class="top-bar">
				<h1>Bulletin Board</h1>
				<div class="breadcrumbs"><a href="#">Bulletin</a> / <a href="#">Entry Page</a></div>
			</div><br />
			<form id="form1" action="" method="post">
				<div class="select-bar-top">
					<input type="hidden" name="action" id="action" />
					<a href="bulletin.php" title="Reload" class="refresh" />Refresh</a>
					<a href="javascript:fnSave();" title="Save" class="save" />Save</a>
				</div>
				<div class="table">
					<img src="images/bg-th-left.gif" width="8" height="7" alt="" class="left" />
					<img src="images/bg-th-right.gif" width="7" height="7" alt="" class="right" />
					<table class="listing form" cellpadding="0" cellspacing="0">
						<tr>
							<th class="full" colspan="2">Bulletin Entry</th>
						</tr>
						<tr>
							<td class="first"><strong>Title</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtTitle" id="txtTitle" class="text" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Message</strong></td>
							<td class="last"><textarea maxlength="200" rows="4" name="txtMessage" id="txtMessage" class="textArea" /></textarea></td>
						</tr>
					</table>
					<p>&nbsp;</p>
					<p><?php echo $afterMessage ?></p>
				</div>
			</form>
		</div>
		<div id="right-column">
			<strong class="h">INFO</strong>
			<div class="box">This is bulletin page. Here any user can add small and important message and hence it is shown in ticker at left side.</div>
	  </div>
	</div>
	<div id="footer">
		IMS 2012 &copy; Developed By Er.Sajish Shilpakar.
		Follow me at: <a href="http://www.sajish.com.np" target="blank">www.sajish.com.np</a>
		<a href="http://www.facebook.com/sajish" target="blank" title="Sajish Facebook" class="fb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>
</body>
</html>
