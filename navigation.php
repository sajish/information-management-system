<div id="header">
	<span class="login">Welcome to IMS portal, <span class="loginName"><?php echo $_SESSION['UserName'] ?></span></span>
	<a href="logout.php" class="logout">Log Out</a>
	<ul id="top-navigation">
		<li <?php if($thisPage=="Home") echo " class=\"active\""; ?>><span><span><a href="home.php">Home</a></span></span></li>
		<?php 
			if($_SESSION['UserType']=="1"){?>
			<li <?php if ($thisPage=="User") echo " class=\"active\""; ?>><span><span><a href="user.php">Users</a></span></span></li>
			<li <?php if ($thisPage=="Courses") echo " class=\"active\""; ?>><span><span><a href="courses.php">Courses</a></span></span></li>
		<?php }?>
		<li <?php if ($thisPage=="Student") echo " class=\"active\""; ?>><span><span><a href="student.php">Students</a></span></span></li>
		<li <?php if ($thisPage=="Bulletin") echo " class=\"active\""; ?>><span><span><a href="bulletin.php">Bulletin Board</a></span></span></li>
	</ul>
</div>