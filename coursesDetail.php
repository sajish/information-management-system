<?php
	//error_reporting(0); 
	session_start();
	require_once('connection.php');
	require_once('commonFunctions.php');
	CheckSession(1);
	$thisPage = "Courses";
	$strIuQuery="";
	
	$courseId = isset($_GET["courseId"]) ? Sanitize($_GET["courseId"]) : '0'; 
	$action = isset($_POST['action']) ? Sanitize($_POST['action']) : ''; 
	$reqName = isset($_POST['txtName']) ? Sanitize($_POST['txtName']) : ''; 
	$reqDescription = isset($_POST['txtDescription']) ? Sanitize($_POST['txtDescription']) : ''; 
	$reqPrice = isset($_POST['txtPrice']) ? Sanitize($_POST['txtPrice']) : ''; 
	
	if($action == "save")
	{
		if($courseId == "0")
		{
			$strIuQuery = "INSERT INTO Courses(Name,Description,Price)";
			$strIuQuery .= " VALUES('".$reqName."','".$reqDescription."','".$reqPrice."')";
			
			mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
			$courseId = mysql_insert_id();
			header("location: coursesDetail.php?courseId=".$courseId);
			exit();
		}
		else
		{
			$strIuQuery = "UPDATE Courses SET Name='".$reqName."',Description='".$reqDescription."',Price='".$reqPrice."'";
			$strIuQuery .= " WHERE CourseId = ".$courseId;
			mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		}
	}
	else if($action == "delete")
	{
		$strIuQuery = "DELETE FROM Courses WHERE CourseId = ".$courseId;
		mysql_query($strIuQuery) or die($strIuQuery."<br/><br/>".mysql_error());
		header("location: Courses.php");
		exit();
	}
	
	// Main Select Query
	$strQuery = "SELECT * FROM Courses WHERE CourseId = ". $courseId;
	
	$result = mysql_query($strQuery) or die($strQuery."<br/><br/>".mysql_error());
	$row = mysql_fetch_array($result);
	
	$courseName = $row["Name"];
	$courseDescription = $row["Description"];
	$coursePrice = $row["Price"];	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Course Detail</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="css/mainStyle.css" />	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.totemticker.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript">
		function fnSave()
		{
			$("#form1").validate({
				rules: {
					txtName: "required",
					txtPrice: {
						required: true,
						number:true
					}
				},
				messages: {
					txtName: "Please enter Course Name",
					txtPrice: {
						required: "Please enter Price"
					}
				}
			});
			
			fnAction('save');
		}
		$(document).ready(function() {
			ShowBulletin(20);
			Ticker();
		});
	</script>
</head>
<body>
<div id="main">
	<?php require_once('navigation.php')?>
	<div id="middle">
		<div id="left-column">
			<h3>Bulletin Ticker</h3>
			<ul id="vertical-ticker">
				<div id="divBulletin">
				</div>
			</ul>
			<h3><a href="#" id="ticker-previous">Previous</a> &nbsp;&nbsp;&nbsp;<a href="#" id="ticker-next">Next</a></h3>
		</div>
		<div id="center-column">
			<div class="top-bar">
				<h1>Courses</h1>
				<div class="breadcrumbs"><a href="#">Course</a> / <a href="#">Detail Page</a></div>
			</div><br />
			<form id="form1" action="" method="post">
				<div class="select-bar-top">
					<input type="hidden" name="action" id="action" />
					<a href="javascript:window.location.href='courses.php';" title="Back To List" class="back" />Back</a>
					<a href="coursesDetail.php?courseId=<?php echo $courseId ?>" title="Reload" class="refresh" />Refresh</a>
					<a href="javascript:fnSave();" title="Save" class="save" />Save</a>
					<a href="javascript:fnAction('delete');" title="Delete" class="delete" />Delete</a>
					<a href="javascript:fnDetail('coursesDetail.php','courseId=0');" title="Add New" class="addNew" />Add</a>
				</div>
				<div class="table">
					<img src="images/bg-th-left.gif" width="8" height="7" alt="" class="left" />
					<img src="images/bg-th-right.gif" width="7" height="7" alt="" class="right" />
					<table class="listing form" cellpadding="0" cellspacing="0">
						<tr>
							<th class="full" colspan="2">Course Details</th>
						</tr>
						<tr>
							<td class="first"><strong>Course Name</strong></td>
							<td class="last"><input type="text" maxlength="50" name="txtName" class="text" value="<?php echo $courseName ?>" /></td>
						</tr>
						<tr class="bg">
							<td class="first"><strong>Course Price Rs.</strong></td>
							<td class="last"><input type="text" maxlength="30" name="txtPrice" id="txtPrice" class="text" value="<?php echo $coursePrice ?>" /></td>
						</tr>
						<tr>
							<td class="first"><strong>Course Description</strong></td>
							<td class="last"><textarea maxlength="200" rows="4" name="txtDescription" id="txtDescription" class="textArea" /><?php echo $courseDescription ?></textarea></td>
						</tr>
					</table>
					<p>&nbsp;</p>
				</div>
			</form>
		</div>
		<div id="right-column">
			<strong class="h">INFO</strong>
			<div class="box">Detail page for the course. Here you can add/edit/delete certain course. You can also assign course price and general description too.</div>
	  </div>
	</div>
	<div id="footer">
		IMS 2012 &copy; Developed By Er.Sajish Shilpakar.
		Follow me at: <a href="http://www.sajish.com.np" target="blank">www.sajish.com.np</a>
		<a href="http://www.facebook.com/sajish" target="blank" title="Sajish Facebook" class="fb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
	</div>
</div>


</body>
</html>


