### Read Me ###
### What is this repository for? ###

This project was created for learning purpose by Sajish Shilpakar in year 2011. 
Hope this project would help students or individual for their assessment and as a learning material. Your can modify or do whatever you want as it fits you. If you like it or have any questions, let me know.

P.S: I will not be updating or making changes on sources anymore. Use it at your own risk.

Demo URL: http://ims.code4beers.com/

Username: admin

Password: ims2011

### How do I get set up? ###

Set up a virtual directory in WAMP/XAMPP etc.

Import database from folder "database" in MySQL/PHPMyAdmin.

Change database username, password in connection.php.

Username and password is same as above.